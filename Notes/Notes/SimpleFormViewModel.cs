﻿using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Math;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Notes
{
    public class SimpleFormViewModel
    {
        public string Note { get; set; }
        

        string _fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),"notes.txt");


        public ICommand SaveCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public SimpleFormViewModel()
        {
            
             DeleteCommand = new Command(get=> DeleteData());
             SaveCommand = new Command(get=>SaveData());

            if (File.Exists(_fileName))
            {
                Note = File.ReadAllText(_fileName);
            }

        }

     
        

        private  void SaveData()
        {
            File.WriteAllText(_fileName,Note.ToLower());
            Application.Current.MainPage.DisplayAlert("Notification", "Your Note is Saved","OK");
        }
        private async void DeleteData()
        {

            bool answer = await Application.Current.MainPage.DisplayAlert("Alert !", "Are you sure to Delete the note", "Yes", "No");
            if (answer == true)
            {
                if (File.Exists(_fileName))
                {
                    File.Delete(_fileName);
                }
                Note = "";
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Notification !", "Deleting Canceled", "OK");

            }

        }

      




    }

}
