﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notes
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SimpleForm : ContentPage
    {
        public SimpleForm()
        {
            InitializeComponent();
            //Binding ViewModel to View Here
            BindingContext = new SimpleFormViewModel();
           
        }
    }
}